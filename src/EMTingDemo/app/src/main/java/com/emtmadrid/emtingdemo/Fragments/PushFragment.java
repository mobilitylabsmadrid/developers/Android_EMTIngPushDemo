package com.emtmadrid.emtingdemo.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;
import com.emtmadrid.emtingdemo.Code.Device;
import com.emtmadrid.emtingdemo.Parameters;
import com.emtmadrid.emtingdemo.ParametersActivity;
import com.emtmadrid.emtingdemo.R;
import com.emtmadrid.emtingdemo.Service.OpenApiService;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;


public class PushFragment extends Fragment {

    private static View view;

    private OnFragmentInteractionListener mListener;

    public PushFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_push, container, false);

        view.findViewById(R.id.btShowParameters).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view2) {

                Intent intent = new Intent(getActivity(), ParametersActivity.class);
                startActivity(intent);

            }
        });

        view.findViewById(R.id.btRegisterDevice).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view2) {
                try {

                    Retrofit retrofit = new Retrofit.Builder().baseUrl(Parameters.API_URL).build();
                    OpenApiService openApiService = retrofit.create(OpenApiService.class);


                    try {

                        RequestBody body = RequestBody.create(MediaType.parse("application/json"), Device.getInstance(getContext()).toJsonRegisterDevice().toString());

                        Call<ResponseBody> call = openApiService.register(Parameters.OPENAPI_ACCESS_TOKEN, body);
                        call.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                                try {

                                    String jsonResponse = response.body().string();

                                    JsonObject jsonObject = new JsonParser().parse(jsonResponse).getAsJsonObject();

                                    Toast.makeText(getContext(), jsonObject.get("description").getAsString(), Toast.LENGTH_LONG).show();


                                } catch (Exception e) {
                                    Toast.makeText(getContext(), "ERROR", Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                Toast.makeText(getContext(), "ERROR PUSH SERVICE", Toast.LENGTH_LONG).show();
                            }
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });

        view.findViewById(R.id.btShowSend).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view2) {
                view.findViewById(R.id.btShowSend).setVisibility(View.GONE);
                view.findViewById(R.id.sendPanel).setVisibility(View.VISIBLE);
            }
        });

        view.findViewById(R.id.btTestNotification).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view2) {

                TextView alertTitle = view.findViewById(R.id.alertTitle);
                TextView alertBody = view.findViewById(R.id.alertBody);
                TextView dataPayload = view.findViewById(R.id.dataPayload);

                String notificationData = "{ \n" +
                        "   \"applicationId\":\"" + Parameters.EMTING_APPLICATIONID + "\",\n" +
                        "   \"channelId\":\"\",\n" +
                        "   \"userId\":\"\",\n" +
                        "   \"deviceId\":\"\",\n" +
                        "   \"idDevice\":\"\",\n" +
                        "   \"alert\":{ \n" +
                        "      \"title\":\"" + alertTitle.getText().toString() + "\",\n" +
                        "      \"body\":\"" + alertBody.getText().toString() + "\"\n" +
                        "   },\n" +
                        "   \"data\":" + dataPayload.getText().toString() + "}";

                Retrofit retrofit = new Retrofit.Builder().baseUrl(Parameters.API_URL).build();
                OpenApiService openApiService = retrofit.create(OpenApiService.class);

                try {

                    RequestBody body = RequestBody.create(MediaType.parse("application/json"), notificationData);

                    Call<ResponseBody> call = openApiService.sendNotification(Parameters.OPENAPI_ACCESS_TOKEN, body);
                    call.enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                            try {

                                String jsonResponse = response.body().string();

                                JsonObject jsonObject = new JsonParser().parse(jsonResponse).getAsJsonObject();

                                Toast.makeText(getContext(), jsonObject.get("description").getAsString(), Toast.LENGTH_LONG).show();

                            } catch (Exception e) {
                                Toast.makeText(getContext(), "ERROR", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Toast.makeText(getContext(), "ERROR PUSH SERVICE", Toast.LENGTH_LONG).show();
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });

        TextView dataPayload = view.findViewById(R.id.dataPayload);
        dataPayload.setText("{\"code\":4}");

        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}


