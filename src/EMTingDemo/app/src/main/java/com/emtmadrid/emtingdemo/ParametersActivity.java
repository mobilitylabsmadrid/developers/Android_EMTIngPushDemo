package com.emtmadrid.emtingdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.emtmadrid.emtingdemo.Code.Device;

public class ParametersActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parameters);

        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Device dev = Device.getInstance(getApplicationContext());

        ((TextView) findViewById(R.id.tvToken)).setText(Parameters.FIREBASE_TOKEN);
        ((TextView) findViewById(R.id.tvApiKey)).setText(Parameters.EMTING_APPLICATIONID);
        //((TextView) findViewById(R.id.tvUser)).setText(Parameters.EMTING_USERNAME);
        ((TextView) findViewById(R.id.tvDeviceId)).setText(dev.getDeviceId());
        ((TextView) findViewById(R.id.tvDevice)).setText(dev.toString());
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}
