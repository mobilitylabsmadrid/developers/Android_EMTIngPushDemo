package com.emtmadrid.emtingdemo.Notifications;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class NotificationContent {

    public static List<NotificationContent.NotificationItem> getListNotifications(Context context) {
        try {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            String jsonListBuffer = sharedPreferences.getString("ListNotifications", null);
            Gson gson = new Gson();
            List<NotificationContent.NotificationItem> listNotifications = new ArrayList<>();

            if (jsonListBuffer != null && jsonListBuffer != "") {
                Type listType = new TypeToken<List<NotificationItem>>() {
                }.getType();
                listNotifications = gson.fromJson(jsonListBuffer, listType);
            }

            return listNotifications;

        } catch (Exception ex) {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            sharedPreferences.edit().clear().apply();
            return new ArrayList<NotificationContent.NotificationItem>();
        }
    }


    public static class NotificationItem {
        public String moment;
        public String alert;
        public String dataPayload;

        public NotificationItem() {
        }

        public NotificationItem(String moment, String alert, String dataPayload) {
            this.moment = moment;
            this.alert = alert;
            this.dataPayload = dataPayload;
        }

        @Override
        public String toString() {
            return dataPayload;
        }
    }
}
