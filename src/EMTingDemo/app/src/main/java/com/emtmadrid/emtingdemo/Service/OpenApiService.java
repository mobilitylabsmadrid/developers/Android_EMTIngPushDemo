package com.emtmadrid.emtingdemo.Service;

import com.google.gson.JsonObject;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface OpenApiService {


    @GET("user/login")
    Call<ResponseBody> login(@Header("X-ClientId") String clientId, @Header("passKey") String pass);

    @Headers("Content-Type: application/json")
    @POST("push/devices/register/")
    Call<ResponseBody> register(@Header("accessToken") String accessToken, @Body RequestBody device);

    @Headers("Content-Type: application/json")
    @POST("push/sendmessage/")
    Call<ResponseBody> sendNotification(@Header("accessToken") String accessToken, @Body RequestBody notification);

}
