package com.emtmadrid.emtingdemo.Fragments;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emtmadrid.emtingdemo.Fragments.NotificationItemFragment.OnListFragmentInteractionListener;
import com.emtmadrid.emtingdemo.Notifications.NotificationContent;
import com.emtmadrid.emtingdemo.R;

import java.util.List;


public class MyNotificationItemRecyclerViewAdapter extends RecyclerView.Adapter<MyNotificationItemRecyclerViewAdapter.ViewHolder> {

    private final List<NotificationContent.NotificationItem> mValues;

    private final OnListFragmentInteractionListener mListener;

    public MyNotificationItemRecyclerViewAdapter(List<NotificationContent.NotificationItem> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_notification_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.item_moment.setText(mValues.get(position).moment);
        holder.item_alert.setText(mValues.get(position).alert);
        holder.item_dataPayload.setText("data: " + mValues.get(position).dataPayload);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView item_moment;
        public final TextView item_alert;
        public final TextView item_dataPayload;
        public NotificationContent.NotificationItem mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            item_moment = view.findViewById(R.id.item_moment);
            item_alert = view.findViewById(R.id.item_alert);
            item_dataPayload = view.findViewById(R.id.item_dataPayload);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + item_dataPayload.getText() + "'";
        }
    }
}
