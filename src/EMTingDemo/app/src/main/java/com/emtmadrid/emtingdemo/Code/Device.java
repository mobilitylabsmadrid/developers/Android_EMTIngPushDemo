package com.emtmadrid.emtingdemo.Code;

import android.content.Context;
import android.provider.Settings;

import com.emtmadrid.emtingdemo.Parameters;
import com.google.gson.JsonObject;


public class Device {

    private String deviceId;
    private DeviceOS os;
    private String manufacturer;
    private String modelNumber;
    private String modelName;

    public static Device getInstance(Context ctx) {
        Device device = new Device();

        device.setDeviceId(Settings.Secure.getString(ctx.getContentResolver(), Settings.Secure.ANDROID_ID));
        device.setManufacturer(android.os.Build.MANUFACTURER);
        device.setModelName(android.os.Build.MODEL);
        device.setModelNumber(android.os.Build.getRadioVersion());

        DeviceOS deviceOS = new DeviceOS();
        deviceOS.setVersion(android.os.Build.VERSION.RELEASE);
        deviceOS.setCompilationNumber(android.os.Build.ID);
        device.setOs(deviceOS);

        return device;
    }


    public JsonObject toJsonRegisterDevice() {

        JsonObject jsonObject = new JsonObject();

        jsonObject.addProperty("applicationId", Parameters.EMTING_APPLICATIONID);
        jsonObject.addProperty("userId", "null");
        jsonObject.addProperty("deviceId", getDeviceId());
        jsonObject.addProperty("manufacturer", getManufacturer());
        jsonObject.addProperty("modelName", getModelName());
        jsonObject.addProperty("modelNumber", getModelNumber());
        jsonObject.addProperty("notificationToken", Parameters.FIREBASE_TOKEN);
        jsonObject.add("os", getOs().toJSON());

        return jsonObject;

    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(String modelNumber) {
        this.modelNumber = modelNumber;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public DeviceOS getOs() {
        return os;
    }

    public void setOs(DeviceOS os) {
        this.os = os;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    @Override
    public String toString() {
        return "deviceId='" + deviceId + '\'' +
                ",\n os=" + os +
                ",\n manufacturer='" + manufacturer + '\'' +
                ",\n modelNumber='" + modelNumber + '\'' +
                ",\n modelName='" + modelName;
    }
}
