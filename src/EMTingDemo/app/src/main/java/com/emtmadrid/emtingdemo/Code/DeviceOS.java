package com.emtmadrid.emtingdemo.Code;


import com.google.gson.JsonObject;

public class DeviceOS {

    private String type = "android";
    private String version;
    private String compilationNumber;


    //Método que convierte el objeto DeviceOS a JSONObject.
    public JsonObject toJSON() {

        JsonObject jsonObject = new JsonObject();

        jsonObject.addProperty("type", getType());
        jsonObject.addProperty("version", getVersion());
        jsonObject.addProperty("compilationNumber", getCompilationNumber());

        return jsonObject;
    }


    //Getter y setter....
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getCompilationNumber() {
        return compilationNumber;
    }

    public void setCompilationNumber(String compilationNumber) {
        this.compilationNumber = compilationNumber;
    }

    @Override
    public String toString() {
        return "DeviceOS{" +
                "type='" + type + '\'' +
                ", version='" + version + '\'' +
                ", compilationNumber='" + compilationNumber + '\'' +
                '}';
    }
}
