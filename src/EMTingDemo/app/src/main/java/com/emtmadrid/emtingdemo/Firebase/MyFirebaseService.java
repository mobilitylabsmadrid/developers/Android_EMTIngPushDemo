package com.emtmadrid.emtingdemo.Firebase;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.text.format.DateFormat;
import android.util.Log;

import com.emtmadrid.emtingdemo.MainActivity;
import com.emtmadrid.emtingdemo.Notifications.NotificationContent;
import com.emtmadrid.emtingdemo.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

public class MyFirebaseService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.d(TAG, "From: " + remoteMessage.getFrom());

        NotificationContent.NotificationItem item = new NotificationContent.NotificationItem();
        item.moment = DateFormat.format("dd-MM-yyyy hh:mm:ss", Calendar.getInstance().getTime()).toString();

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message Data payload: " + remoteMessage.getData());
            sendNotification("EMTing Data payload","Data: " + remoteMessage.getData());
            item.dataPayload = remoteMessage.getData().toString();
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification received: " + remoteMessage.getNotification().getTitle() + " - " + remoteMessage.getNotification().getBody());
            sendNotification("EMTing Notification received",remoteMessage.getNotification().getTitle() + " - " + remoteMessage.getNotification().getBody());
            item.alert = remoteMessage.getNotification().getTitle() + " - " + remoteMessage.getNotification().getBody();
        }

        Gson gson = new Gson();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        String jsonListBuffer = sharedPreferences.getString("ListNotifications", null);

        List<NotificationContent.NotificationItem> listNotifications = new ArrayList<NotificationContent.NotificationItem>();

        if (jsonListBuffer != null && jsonListBuffer != "")
        {
            Type listType = new TypeToken<List<NotificationContent.NotificationItem>>() {}.getType();
            listNotifications = gson.fromJson(jsonListBuffer, listType);
        }

        listNotifications.add(0,item);

        String jsonList = gson.toJson(listNotifications);

        sharedPreferences.edit().putString("ListNotifications", jsonList).apply();
    }

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    @Override
    public void onNewToken(String token) {
        Log.d(TAG, "Refreshed token: " + token);
        sendRegistrationToServer(token);
    }

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        Log.i(TAG,"sendRegistrationToServer " + token);
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param msgBody FCM message body received.
     */
    private void sendNotification(String msgTitle, String msgBody) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.app_name);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.ic_launcher_round)
                        .setContentTitle(msgTitle)
                        .setContentText(msgBody)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        Random random = new Random();
        int m = random.nextInt(9999 - 1000) + 1000;
        notificationManager.notify(m, notificationBuilder.build());
    }
}
