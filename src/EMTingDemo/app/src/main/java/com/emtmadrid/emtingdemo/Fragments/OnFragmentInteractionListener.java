package com.emtmadrid.emtingdemo.Fragments;

import android.net.Uri;

public interface OnFragmentInteractionListener {
    void onFragmentInteraction(Uri uri);

}
