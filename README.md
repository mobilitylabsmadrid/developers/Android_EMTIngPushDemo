# EMTingPushDemo

Proyecto de ejemplo para mostrar como enviar y recibir notificaciones push a través del API Rest de MobilityLabs Madrid (EMTMadrid).

* [MobilityLabs Madrid](https://mobilitylabs.emtmadrid.es/)
* [API Reference](https://apidocs.emtmadrid.es/)

Para el uso del API Rest hay que darse de alta en MobilityLabs.

Para usar el Servicio de Notificaciones Push hay que crear una aplicación EMTing en MobilityLabs.

* [Manual para crear una aplicación en EMTing](https://mobilitylabs.emtmadrid.es/doc/new-app)

### Instrucciones de configuración de la APP

Modificar en el archivo Parameters.java las propiedades con datos de acceso a MobiltyLabs y con el AplicationId de la aplicación creada:

```java
public static String EMTING_APPLICATIONID;
public static String EMTING_USERNAME;
public static String EMTING_PASS;
```

Hay que reemplazar el archivo el archivo **google-services.json** por el que se genere de una aplicación en Firebase.
![Img4](./doc/Screenshot_4.png)


### EMTing Push Demo

![Img1](./doc/Screenshot_1.png)

![Img2](./doc/Screenshot_2.png)

![Img3](./doc/Screenshot_3.png)
